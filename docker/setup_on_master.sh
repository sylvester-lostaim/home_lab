#!/bin/bash

set -xe

registry_mount=/mnt/registry

# create dummy CA key & cert with empty info
openssl req -newkey rsa:4096 -nodes -sha256 -keyout ca.key -x509 -days 10000 -out ca.crt -subj "/C=. /ST=. /L=. /O=. /OU=. /CN=test" >/dev/null 2>&1

mkdir -p $registry_mount/{images,ca,auth}
chown -R root:root $registry_mount
chmod -R 600 $registry_mount
mv ca.key ca.crt $registry_mount/ca

docker-compose -f registry_server.yml up -d --no-recreate --timeout 20
docker exec --interactive --tty registry_server sh -c 'htpasswd -Bbn test_user test_pw > /etc/registry/auth/htpasswd'

# bug fixes for `docker login`
# Error response from daemon: Get http://localhost:3333/v2/: EOF
apt-get install -y pass
gpg2 --pinentry-mode=loopback --passphrase "" --batch --gen-key bugfix_gpg2_creation 
pass init `gpg2 --list-keys | grep -B1 '<aaaaa@a.a>' | head -1`
