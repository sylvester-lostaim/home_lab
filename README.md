# home_lab
```yaml
  # Ansible bugs as unable to obtain full distribution version for Debian and CentOS
  # reproducible by running `ansible -m setup [hostname] | grep "ansible_distribution.*version"`
  #when: ansible_facts['distribution'] == "Debian" and ansible_facts['distribution_version'] == '10.3'
  when: ansible_facts['distribution'] == "Debian" and ansible_facts['distribution_version'] == '10'

  # Ansible is still having bad handling on systemd service restarting, in its service module
  # command module turns out to be more reliable in this case
  # reproducible by setting up "chrony NTP service"

  # docker bugs as `docker login` raises credential storing issue
  # by forcing to run with graphical interface
  # `Cannot autolaunch D-Bus without X11 $DISPLAY`
```
# Roadmap
- Setup OpenStack, stateful services, persistent volume
- Test out KubeCF

# Emerging from DevOps to NoOps
- SRE, mainly managing resource & service availability.
