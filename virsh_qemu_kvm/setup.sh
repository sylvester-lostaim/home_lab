#!/bin/bash

# create first template
sudo virt-install --virt-type kvm --name test --ram 1024 --vcpus 1 --location 'http://ftp.debian.org/debian/dists/buster/main/installer-amd64/' --disk size=10 --os-type linux --os-variant debian10 --graphics none --extra-args 'console=ttyS0' --check disk_size=off --network network=br10

# suspend to clone disk, then resume again
# virt-clone will feel unhappy, if run parallel in asymmetric, by using xargs
# -P switch is not specified
sudo virsh suspend --domain test
echo {1..4} | tr ' ' '\n' | xargs -I % sh -c "sudo virt-clone -o test -n test% --auto-clone"
sudo virsh resume --domain test

# shutdown all hosts for preparation
# require direct disk-write (prevent disk corruption)
sudo virsh list --all | awk 'NR>2 {print $2}' | xargs -P 5 -I % sh -c "sudo virsh shutdown %"
sleep 10

# start preparation for usable cloned image
# virt-sysprep will help to remove unique info on system
# passwordless ssh
sudo virt-sysprep --mkdir /root/.ssh --copy-in authorized_keys:/root/.ssh/ -d test
echo {1..4} | tr ' ' '\n' | xargs -P 4 -I % sh -c "sudo virt-sysprep --mkdir /root/.ssh --copy-in authorized_keys:/root/.ssh/ -d test%"
# set hostname (cloned image has to be corrected)
echo {1..4} | tr ' ' '\n' | xargs -P 4 -I % sh -c "sudo virt-sysprep --hostname test% -d test%"

# fire up all VM(s) 
sudo virsh list --all | awk 'NR>2 {print $2}' | xargs -P 5 -I % sh -c "sudo virsh start %"

# generate key exchange for openssh-server for passwordless login
sudo virsh net-dhcp-leases br10 | awk 'NR>2 {sub("/24","",$4); print $4}' | xargs -I % -P 5 sh -c "ssh root@% 'dpkg-reconfigure openssh-server'"
