#!/bin/bash

# bootstrap ceph services with podman/docker
#
# starting from Ceph codename Octopus 
# the cephadm itself will handle the global conffile directory path
# to be under /etc/ceph/ (change directory is no longer required)
cephadm bootstrap --mon-ip `hostname -i`

# consume all devices for osd
ceph orch apply osd --all-available-devices

# add desired device
ceph orch daemon add osd test:/dev/vdb
ceph orch daemon add osd test:/dev/vdc

# create CephFS volume for metadata
ceph fs volume create volume_test
ceph orch apply mds volume_test 1

# not apply here
#
# manage it under one realm, one zone, one pool, one pd
#radosgw-admin realm create --rgw-realm=test_org --default
#radosgw-admin zonegroup create --rgw-zonegroup=default --master --default
#radosgw-admin zone create --rgw-zonegroup=default --rgw-zone=test_zone --master --default
#ceph orch apply rgw test_org test_zone
