#!/bin/bash

# mount a empty disk to "test", master VM
# commit 9925a65758a32c059f40574e07d9adc30cb59884 only provide 10G of disk, not enough
qemu-img create -f raw /var/lib/libvirt/images/test_disk2.img 50G
qemu-img create -f raw /var/lib/libvirt/images/test_disk3.img 50G
virsh attach-disk test --source /var/lib/libvirt/images/test_disk2.img --target vdb --persistent
virsh attach-disk test --source /var/lib/libvirt/images/test_disk3.img --target vdc --persistent
