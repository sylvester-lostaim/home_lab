#!/bin/bash

# this script does not handle any re-configuring issues
# ensure this is being run after ansible commit
# all machine are not fulfilling official k8s requirement, self-tuning might require
ansible -m command -a "swapoff" all

# setup bash completion for kubectl command
kubectl completion bash > /etc/bash_completion.d/k8s.sh
source /etc/bash_completion

# master node (control-plane)
# decide to have one control-plane node
# do not allow control-plane node to run with any pods
kubeadm init --pod-network-cidr=172.16.0.0/16 --control-plane-endpoint=test
token=`kubeadm token list | awk 'NR==2 {print $1}'`
ca_cert_hash=`openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'`

# slave node
ansible -m command -a "kubeadm join test:6443 --control-plane --token $token --discovery-token-ca-cert-hash sha256:$ca_cert_hash" \!master
