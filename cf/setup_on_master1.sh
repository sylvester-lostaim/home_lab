#!/bin/bash

# this script does not handle any re-configuring issues
# ensure this is being run after ansible commit
# all machine are not fulfilling official OpenStack+CloudFoundry requirement, self-tuning might require

# get bosh-cli
bosh_cli_sha256hash='ca7580008abfd4942dcb1dd6218bde04d35f727717a7d08a2bc9f7d346bce0f6'
touch /usr/local/bin/bosh-cli
if [ `sha256sum /usr/local/bin/bosh-cli | cut -f1 -d' '` != $bosh_cli_sha256hash ];then
  wget -O /usr/local/bin/bosh-cli https://github.com/cloudfoundry/bosh-cli/releases/download/v6.2.1/bosh-cli-6.2.1-linux-amd64
  chmod +x /usr/local/bin/bosh-cli
fi

# install bosh-cli dependencies to fulfill BOSH bootstrap requirement
apt-get install -y build-essential zlibc zlib1g-dev ruby ruby-dev openssl libxslt1-dev libxml2-dev libssl-dev libreadline7 libreadline-dev libyaml-dev libsqlite3-dev sqlite3

# generate X.509 certificate for authentication use
openstack keypair create cf_test --private-key cf_test_private_key.pem

# create new network for cloudfoundry purpose and assign floating ip for it
# the kvm dhcp will take the range until 192.168.3.200
# take /28 as CIDR, started from 192.168.3.208
# set subnet pool quota will only accept exact number of ip range
openstack network create --project `openstack project list | awk '/admin/ {print $2}'` --provider-network-type vxlan --project-domain Default --external --description "cloud foundry use" cf_test
openstack subnet pool create --project `openstack project list | awk '/admin/ {print $2}'` --project-domain Default --pool-prefix 192.168.3.0/24 --description "cloud foundry use" --default-quota 16 cf_test_subnet_pool
openstack subnet create --project `openstack project list | awk '/admin/ {print $2}'` --project-domain Default --subnet-pool cf_test_subnet_pool --network cf_test --description "cloud foundry use" --subnet-range 192.168.3.208/28 cf_test_subnet
openstack floating ip create --project `openstack project list | awk '/admin/ {print $2}'` --project-domain Default --floating-ip-address 192.168.3.209 --description 'cloud foundry use' cf_test

# get bosh-deployment repository
dpkg -l | grep -qE "^ii *git " || apt-get install -y git
git clone https://github.com/cloudfoundry/bosh-deployment
cd bosh-deployment
git checkout 5c16f7dedd2de2ff9b6e21ca8d9572e3b4fa32c8
cd ..

echo 'please provide matched value to "net_id" in "vars.yml" file before executing "setup_on_master2.sh"'
