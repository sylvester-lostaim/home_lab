#!/bin/bash

# this script does not handle any re-configuring issues
# ensure this is being run after ansible commit
# all machine are not fulfilling official OpenStack+CloudFoundry requirement, self-tuning might require

bosh create-env bosh-deployment/bosh.yml --state=bosh-state.json --vars-store=director-creds.yml --vars-file=vars.yml -o bosh-deployment/openstack/cpi.yml -o bosh-deployment/external-ip-with-registry-not-recommended.yml --var-file=private_key=cf_test_private_key.pem
