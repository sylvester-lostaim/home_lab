# Environment
- At this point of time, the setup and configuration will not be written into Ansible Playbook.
    - please run "setup_on_master1.sh", then "setup_on_master2.sh"
    - please provide matched value to "net_id" in "vars.yml" file before executing "setup_on_master2.sh"
- Please visit [official bosh repository], for different underlying IaaS setup or recommended setup in production environment.
- The default security group will be used by cf (in this environment), which __DOES NOT APPLY__ to any good security practice,
    - See "openstack_config" output file in this directory:
        - openstack security group list
        - openstack security group rule list default
- You may subscribe to HashiCorp product, Terraform to have automated setup. However, by running "setup_on_master.sh" script in configuration, you should still expect to get following setup:
    - See "openstack_config" output file in this directory:
        - openstack keypair show cf_test
        - openstack network show cf_test
        - openstack subnet pool show cf_test_subnet_pool
        - openstack subnet show cf_test_subnet
        - openstack floating ip show 192.168.3.209

[official bosh repository]: <https://github.com/cloudfoundry/bosh-deployment>
