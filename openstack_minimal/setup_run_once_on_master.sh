#!/bin/bash

# this script does not handle any re-configuring issues
# ensure this is being run after ansible commit

# prevent using similar distributed key store path
# that will conflict with k8s
# k8s service has to be restarted and interrupted
sed -i "s#/var/lib/etcd#/var/lib/k8s_etcd#" /etc/kubernetes/manifests/etcd.yaml

# postgresql 
# openstack service will depend on DB to manage/store its data
cp psql_command /var/lib/postgresql
chmod +r /var/lib/postgresql/psql_command
su - postgres -c 'psql < psql_command'
rm /var/lib/postgresql/psql_command

# OpenStack will access rabbitmqctl as openstack user
# status exchanging purpose
# openstack user will need permission to configure & read-write
rabbitmqctl add_user openstack openstack_pw
rabbitmqctl set_permissions openstack ".*" ".*" ".*"

# keystone
su - keystone -c 'keystone-manage db_sync'
keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
keystone-manage credential_setup --keystone-user keystone --keystone-group keystone
keystone-manage bootstrap --bootstrap-password openstack_admin_pw --bootstrap-admin-url http://localhost:5000/v3/ --bootstrap-internal-url http://localhost:5000/v3/ --bootstrap-public-url http://localhost:5000/v3/ --bootstrap-region-id regiontest

# glance
openstack service create --name glance --description "VM Image" image
openstack endpoint create --region regiontest image public http://localhost:9292
openstack endpoint create --region regiontest image internal http://localhost:9292
openstack endpoint create --region regiontest image admin http://localhost:9292
su - glance -c 'glance-manage db_sync'
# add test iso file, use 'bare' as not container or metadata envelope 
# this iso is less than 1MB, and comfortable for testing use
wget -O test.iso http://ftp.sh.cvut.cz/slax/Slax-9.x/slax-ipxe.iso
glance image-create --name "test_image_qcow2" --file test.iso --disk-format qcow2 --container-format bare --visibility public
rm test.iso

# placement
# the service can be tested on different api version with commands below:
#   // for cpu, memory, disk space, network:
#     openstack --os-placement-api-version 1.2 resource class list
#   // for cpu, gpu, nic hardwares or COMPUTE type:
#   openstack --os-placement-api-version 1.6 trait list
openstack service create --name placement --description "Resource Manager" placement
openstack endpoint create --region regiontest placement public http://localhost:8778
openstack endpoint create --region regiontest placement internal http://localhost:8778
openstack endpoint create --region regiontest placement admin http://localhost:8778
su - placement -c 'placement-manage db sync'

# nova
openstack service create --name nova --description "Compute Service" compute
openstack endpoint create --region regiontest compute public http://localhost:8774/v2.1
openstack endpoint create --region regiontest compute internal http://localhost:8774/v2.1
openstack endpoint create --region regiontest compute admin http://localhost:8774/v2.1
# populate db based on conffile
su - nova -c 'nova-manage api_db sync'
# setup cell to scale distributedly
su - nova -c 'nova-manage cell_v2 map_cell0 --database_connection postgresql+psycopg2://nova_user:nova_pw@localhost/nova_inactive_cell_db'
su - nova -c 'nova-manage cell_v2 create_cell --name=cell_1'
su - nova -c 'nova-manage db sync'

# neutron
openstack service create --name neutron --description "Network Service" network
openstack endpoint create --region regiontest network public http://localhost:9696
openstack endpoint create --region regiontest network internal http://localhost:9696
openstack endpoint create --region regiontest network admin http://localhost:9696
su - neutron -c 'neutron-db-manage --config /etc/neutron/dhcp_agent.ini --config /etc/neutron/l3_agent.ini --config /etc/neutron/plugins/ml2/linuxbridge_agent.ini --config /etc/neutron/metadata_agent.ini --config /etc/neutron/plugins/ml2/ml2_conf.ini --config /etc/neutron/neutron.conf upgrade head'
