# Before proceeding

Please source the "env.sh" in this directory.

# In this environment

The OpenStack will be setup manually in this environment.

Minimal OpenStack services which planned to deploy in this environment:

- keystone
- glance
- placement
- nova
- neutron
- cinder (not implemented, replaced with swift)
- swift
- magnum
- zun (not implemented, as not running OpenStack services in containers)

# For full features

OpenStack can be installed with Ansible's help. openstack-ansible automated script can be considered.

Full set of dependencies and infrastructures will be setup based on [this playbook].

[this playbook]: <https://github.com/openstack/openstack-ansible/blob/master/playbooks/setup-infrastructure.yml>
