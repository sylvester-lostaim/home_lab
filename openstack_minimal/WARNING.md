# Environment

* low resource laptop 
    ```
    CPU: i5-4210U, RAM: 4GB (disk space should not be an issue)
    All services in this Git repository are configured in this environment, and fail to match hardware requirement
    Other undocumented extensive configuration is done, in order to cope with low resources issue
    all configuration is done in VMs, but it is not ideal for OpenStack services
    ```

* Backported OpenStack (Train) repositories from Debian (unstable distribution) is being heavily used in this environment

* Official documentation will recommend MySQL rather than Postgresql that being used in this environment

* neutron self-service type networks is configured, but it shouldn't run in virtual network like this environment
    ```
    layer-3 will be supported, and able to run more advanced services like firewall-as-a-service & others
    linuxbridge mechanism driver is configured in this environment, please alter to openvswitch to support mass vnet(s)
    ```

* Commonly, the cinder should also setup for qemu-kvm storage consumption, but is not able to achieve in this environment

# Configuration

- Please aware some services require user to configure on both controller and compute nodes.
    ```
    At this point of time,
    compute nodes will only be configured after all services are verified on controller
    in this manual configuration. (See "openstack_config" for some expected outcomes)
    ```

- No ACL or privilege separation is considered in this home lab.

- It is not recommended to install k8s and OpenStack on same node, as there will be having quite some conflicts in configuration.
    ``` 
    Please avoid if less experience in tuning these two services 
    ```

- k8s's distributed key storing path will get conflicted, hence native/vanilla k8s configuration on etcd will be altered
    ``` 
    configured by script, "setup_run_once_in_master.sh"
    ```

- k8s distributed key ports = 23XX
    ```
    etcd service ports had been switched from 23XX to 24XX, where configuration file is located at /etc/default/etcd, 
    configured by Ansible, via "master" role
    ```

- docker registry port, 5000 (older version of keystone will require 2 listening ports)
    ```
    this may conflict with identity service (keystone) port
    there are 2 workarounds here:
      (1) rewrite UWSGI_PORT number in service file (/etc/init.d/keystone)
      (2) reconfigure docker port routing from host to container
    In this environment, (2) is chosen, where Ansible configuration & docker-compose xml is altered in this case
    As (1) is simple but dirty,
      - There is a chance for service file to be rewritten, whenever package upgrade is performed
      - Altering conffile is more intuitive than service file, let's avoid from disasterous handover task! :3
    ```
